# PSB

Not actually a KiriKiri Z file format, but a large amount of KiriKiri Z games use [E-mote SDK](https://emote.mtwo.co.jp/), which utilizes PSB files.

## References
- [FreeMote](https://github.com/UlyssesWu/FreeMote)
- [psbfile](https://github.com/number201724/psbfile)
