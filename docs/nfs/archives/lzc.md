# LZC

There are two kinds of LZC files:

- File header + (n) chunks
- Chunk header + chunk data

## File Header

| Name | Size | Description |
|---|---|---|
| Magic | 4 bytes | [0x88, 0x33, 0x11, 0x66] |
| Uncompressed size | 4 bytes | Sum of all chunk uncompressed sizes |
| Compressed size | 4 bytes | Sum of all chunk compressed sizes (including chunk headers) |
| Flags | 4 bytes | Usually 0x02 |


## Chunk Header

| Name | Size | Description |
|---|---|---|
| Magic | 4 bytes | [0x22, 0x11, 0x44, 0x55] |
| Uncompressed size | 4 bytes | |
| Compressed size | 4 bytes | includes header (24 bytes) |
| Data offset | 4 bytes | |
| Padding | 8 bytes | |

Chunks are usually 0x8000 bytes large (uncompressed size).
