# GIN / Ginsu / Gnsu20

## File Header

| Name | Size | Description |
|---|---|---|
| Magic | 8 bytes | "Gnsu20\x00\x00" |
| Start RPM | f32 | |
| End RPM | f32 | |
| Entries in Table 1 | u32 | |
| Entries in Table 2 | u32 | |
| Sample count | u32 | |
| Sample rate | u32 | |

## Tooling
- [Takoyaki](https://gitlab.com/sparkserver/takoyaki)
- [GINTool](https://github.com/TheUnpunished/gintool)
