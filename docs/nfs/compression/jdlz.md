# JDLZ

## Header

| Name | Size | Notes |
|---|---|---|
| Magic | 4 bytes | 'JDLZ' in little-endian ([0x4A, 0x44, 0x4C, 0x5A]) |
| Version? | 4 bytes | 0x02 |
| Uncompressed size | 4 bytes |  |
| Compressed size | 4 bytes | includes header (24 bytes) |
| Padding | 8 bytes | |

## Algorithm

Based on LZSS(?).

State consists of two sets of 16-bit flags (fA and fB), both of which are initialized to 0x100.

[Reference decompressor](https://gitlab.com/sparkserver/libworld/-/blob/master/src/compression/jdlz/decompress.rs)
